import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsuarioModule } from './usuario/usuario.module';
import { UsuaruiController } from './usuarui/usuarui.controller';
import { ControllerService } from './controller/controller.service';

@Module({
  imports: [UsuarioModule],
  controllers: [AppController, UsuaruiController],
  providers: [AppService, ControllerService],
})
export class AppModule {}
