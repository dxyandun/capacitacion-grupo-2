# comandos para hacer commit

### Clonar un repositorio
-ejecutamos el siguiente comando

***git clone URL_REPOSITORIO***


### Agregar archivos para enviar

git add nombreArchivo - Para cuando queremos enviar un solo archivo

git add . - Para enviar todos los archivos

-Para hacer Commit

git commit -m "el mensaje"

-Para hacer push

git push origin nombre_rama

git push origin main

-Para traer cambios

git pull

#comandos nest

-crear modulo
    nest generate module nombre_modulo
    se debe crear primero el modulo
-crear controller
    nest generate controller nombre_controller
-crear service
    nest generate service nombre_service